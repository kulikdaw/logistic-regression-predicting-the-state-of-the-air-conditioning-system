# Logistic Regression - predicting the state of the air conditioning system
This project aims to predict the state of air conditioning systems using Logistic regression, a popular machine learning algorithm for binary classification tasks. The dataset provided contains information on pharmaceutical air handling units, which will be used to train and evaluate the models.


## Getting started
pip install scikit-learn pandas numpy matplotlib eli5
pip install jupyter

## Dataset
Dataset  -> https://www.kaggle.com/datasets/vitthalmadane/pharma-air-handling-units-data

## Authors 
Dawid Kulik

## License
This project is licensed for non-commercial use only. For commercial use, please contact the author.

## Project status
This project is part of a master's thesis and has been completed. No further development is planned at this time.
